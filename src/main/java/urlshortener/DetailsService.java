package urlshortener;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import urlshortener.model.Account;
import urlshortener.repository.AccountRepository;

import java.util.Collections;
import java.util.List;

/**
 * Used to retrieve user related data. Used by the DaoAuthenticationProvider to load details about the user during
 * authentication.
 */
@Component
public class DetailsService implements UserDetailsService {

    @Autowired
    AccountRepository accountRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        final Account account = accountRepository.findOne(username);

        if (account == null) {
            throw new UsernameNotFoundException("Account doesn't exist");
        }

        // for simplicity every user has only USER role
        final List<GrantedAuthority> authorities = Collections.singletonList(new SimpleGrantedAuthority("ROLE_USER"));

        return new User(account.getId(), account.getPassword(), authorities);
    }

}
