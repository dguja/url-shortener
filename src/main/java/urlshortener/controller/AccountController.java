package urlshortener.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import urlshortener.dto.AccountResponse;
import urlshortener.model.Account;
import urlshortener.repository.AccountRepository;
import urlshortener.util.StringUtils;

import javax.validation.Valid;

@RestController
public class AccountController {

    private static final int PASSWORD_LENGTH = 8;

    private final AccountRepository accountRepository;

    @Autowired
    public AccountController(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    /**
     * Creates a new account for given accountId if accountId already does not exist.
     *
     * @param accountRequest data transfer object that holds accountId
     * @return JSON object with generated password for accountId
     */
    @RequestMapping(value = "/account",
                    method = RequestMethod.POST,
                    consumes = "application/json",
                    produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity registerAccount(@Valid @RequestBody AccountResponse.AccountRequest accountRequest) {
        Account account = accountRepository.findOne(accountRequest.getAccountId());

        HttpStatus httpStatus;
        // could have been a Map instead of AccountResponse object, discussable what is better
        AccountResponse accountResponse;

        if (account == null) {
            // account does not exist; save to the DB
            final String password = StringUtils.generateAlphanumericString(PASSWORD_LENGTH);
            account = new Account(accountRequest.getAccountId(), password);
            accountRepository.save(account);

            httpStatus = HttpStatus.CREATED;
            accountResponse = new AccountResponse(true /* success */, "Your account is opened", password);
        } else {
            // account already exists
            httpStatus = HttpStatus.CONFLICT;
            accountResponse = new AccountResponse(
                    false /* success */,
                    "AccountId " + accountRequest.getAccountId() + " already exist",
                    null /* password */);
        }

        return ResponseEntity.status(httpStatus).body(accountResponse);
    }
}
