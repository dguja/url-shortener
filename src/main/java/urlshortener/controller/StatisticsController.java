package urlshortener.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import urlshortener.model.LongToShortUrlMapping;
import urlshortener.repository.LongToShortUrlMappingRepository;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

@RestController
public class StatisticsController {

    private final LongToShortUrlMappingRepository longToShortUrlMappingRepository;

    @Autowired
    public StatisticsController(LongToShortUrlMappingRepository longToShortUrlMappingRepository) {
        this.longToShortUrlMappingRepository = longToShortUrlMappingRepository;
    }

    /**
     * Returns a statistic for the accountId
     *
     * @param accountId the accountId for which statistic is returned
     * @return hits count for each long url for the accountId
     */
    @RequestMapping(value = "/statistic/{AccountId}",
                    method = RequestMethod.GET,
                    consumes = "application/json",
                    produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity getStatistic(@PathVariable("AccountId") String accountId) {
        final Collection<LongToShortUrlMapping> urlMappings = longToShortUrlMappingRepository.findByAccountId(accountId);

        final Map<String, Integer> longUrlToRedirectCount = new HashMap<>();
        for (final LongToShortUrlMapping urlMapping : urlMappings) {
            longUrlToRedirectCount.put(urlMapping.getLongUrl(), urlMapping.getHitsCounter());
        }

        return ResponseEntity.ok(longUrlToRedirectCount);
    }
}
