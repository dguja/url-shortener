package urlshortener.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import urlshortener.dto.AccountResponse;
import urlshortener.model.Account;
import urlshortener.model.LongToShortUrlMapping;
import urlshortener.repository.AccountRepository;
import urlshortener.repository.LongToShortUrlMappingRepository;
import urlshortener.util.StringUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.IOException;
import java.util.Collections;

/**
 * Controller is used to generate short url from long url. Redirects a user when short url is visited.
 */
@RestController
public class UrlController {

    private static final String REGISTER_PATH = "/register";

    private final AccountRepository accountRepository;

    private final LongToShortUrlMappingRepository longToShortUrlMappingRepository;

    @Autowired
    public UrlController(AccountRepository accountRepository,
                         LongToShortUrlMappingRepository longToShortUrlMappingRepository) {
        this.accountRepository = accountRepository;
        this.longToShortUrlMappingRepository = longToShortUrlMappingRepository;
    }

    /**
     * Creates a short url and mapping between long and short url.
     *
     * @param urlRegisterRequest data transfer object; contains input arguments for creating a short url
     * @param request            http request
     * @return returns a json object with generated short url
     */
    @RequestMapping(value = REGISTER_PATH,
            method = RequestMethod.POST,
            consumes = "application/json",
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity getShortUrl(@Valid @RequestBody AccountResponse.UrlRegisterRequest urlRegisterRequest, HttpServletRequest request) {
        final Account account = getAuthenticatedUserAccount();

        // check if already exists mapping for longUrl and current account id
        final String longUrl = urlRegisterRequest.getUrl();
        LongToShortUrlMapping longToShortUrlMapping =
                longToShortUrlMappingRepository.findByAccountIdAndLongUrl(account.getId(), longUrl);

        final int redirectType = urlRegisterRequest.getRedirectType();
        if (longToShortUrlMapping == null) {
            // if doesn't exist create new mapping
            final String shortUrlPrefix = getShortUrlPrefix(request);
            longToShortUrlMapping =
                    createLongToShortUrlMapping(longUrl, redirectType, account, shortUrlPrefix);

            // update account object
            account.addUrlMapping(longToShortUrlMapping);
            accountRepository.save(account);
        } else {
            // otherwise just update redirectType
            longToShortUrlMapping.setRedirectType(redirectType);
            longToShortUrlMappingRepository.save(longToShortUrlMapping);
        }

        return ResponseEntity.ok(Collections.singletonMap("shortUrl", longToShortUrlMapping.getShortUrl()));
    }

    /**
     * Redirects a user to the original url using a short url.
     *
     * @param shortUrlSuffix url suffix created on /register
     * @param response       http response
     * @throws IOException exception can be thrown while redirecting a user
     */
    @RequestMapping(value = "/{shortUrlSuffix}", method = RequestMethod.GET)
    public void redirect(@PathVariable String shortUrlSuffix, HttpServletResponse response) throws IOException {
        final LongToShortUrlMapping urlMapping = longToShortUrlMappingRepository.findByShortUrlSuffix(shortUrlSuffix);
        if (urlMapping != null) {
            urlMapping.increaseHitsCounter();
            longToShortUrlMappingRepository.save(urlMapping); // update hits count

            response.setStatus(urlMapping.getRedirectType());
            response.sendRedirect(urlMapping.getLongUrl());
        } else {
            // short url does not exist
            response.sendError(HttpServletResponse.SC_NOT_FOUND);
        }
    }

    /**
     * Returns a prefix, depends on a request URL.
     *
     * @param request http request
     * @return prefix, if url was http://localhost:8080/register, returning value is http://localhost:8080
     */
    private String getShortUrlPrefix(HttpServletRequest request) {
        final String requestUrl = request.getRequestURL().toString();
        return requestUrl.substring(0, requestUrl.indexOf(REGISTER_PATH));
    }

    private LongToShortUrlMapping createLongToShortUrlMapping(String longUrl, int redirectType, Account account,
                                                              String prefixUrl) {
        // first save into the db to get unique Id
        final LongToShortUrlMapping longToShortUrlMapping =
                longToShortUrlMappingRepository.save(new LongToShortUrlMapping(account, longUrl, null, redirectType));

        // encode unique id to the base 62 - this will be unique suffix of a new unique short url
        final String shortUrlSuffix = StringUtils.encode(longToShortUrlMapping.getId());
        longToShortUrlMapping.setShortUrlSuffix(shortUrlSuffix);
        // full new short url is concatenation of prefix (hostname, port...) and generated unique suffix
        longToShortUrlMapping.setShortUrl(prefixUrl + "/" + shortUrlSuffix);

        return longToShortUrlMappingRepository.save(longToShortUrlMapping);
    }

    private Account getAuthenticatedUserAccount() {
        final Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        final String accountId = auth.getName(); //get logged in username
        return accountRepository.findOne(accountId);
    }
}
