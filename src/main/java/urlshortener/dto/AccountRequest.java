package urlshortener.dto;

import org.hibernate.validator.constraints.NotEmpty;

/**
 * Data transfer object. Used to read arguments when creating new account.
 */
public class AccountRequest {

    @NotEmpty(message = "accountId can't be empty")
    private String accountId;

    public AccountRequest() {
    }

    public AccountRequest(String accountId) {
        this.accountId = accountId;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }
}
