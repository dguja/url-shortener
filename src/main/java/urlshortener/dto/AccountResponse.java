package urlshortener.dto;


import com.fasterxml.jackson.annotation.JsonInclude;
import org.hibernate.validator.constraints.NotEmpty;
import org.hibernate.validator.constraints.Range;

/**
 * Data transfer object. Used to send response after creating a new account.
 */
@JsonInclude(JsonInclude.Include.NON_EMPTY) // ignore empty fields when serializing to json
public class AccountResponse {

    private boolean success;

    private String description;

    private String password;

    public AccountResponse(boolean success, String description, String password) {
        this.success = success;
        this.description = description;
        this.password = password;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public static class UrlRegisterRequest {

        private static final int DEFAULT_REDIRECT_TYPE = 302;

        @NotEmpty(message = "url can't be empty")
        private String url;

        @Range(min = 301, max = 302, message = "redirectType should be 301 or 302")
        private int redirectType;

        public UrlRegisterRequest() {
            this(null, DEFAULT_REDIRECT_TYPE); // default values
        }

        public UrlRegisterRequest(String url, int redirectType) {
            this.url = url;
            this.redirectType = redirectType;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public int getRedirectType() {
            return redirectType;
        }

        public void setRedirectType(int redirectType) {
            this.redirectType = redirectType;
        }
    }

    public static class AccountRequest {

        @NotEmpty(message = "accountId can't be empty")
        private String accountId;

        public AccountRequest() {
        }

        public AccountRequest(String accountId) {
            this.accountId = accountId;
        }

        public String getAccountId() {
            return accountId;
        }

        public void setAccountId(String accountId) {
            this.accountId = accountId;
        }
    }
}
