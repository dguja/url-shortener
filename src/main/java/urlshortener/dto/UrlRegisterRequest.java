package urlshortener.dto;

import org.hibernate.validator.constraints.NotEmpty;
import org.hibernate.validator.constraints.Range;

/**
 * Data transfer object. Used to read arguments when creating a short url for the long url.
 */
public class UrlRegisterRequest {

    private static final int DEFAULT_REDIRECT_TYPE = 302;

    @NotEmpty(message = "url can't be empty")
    private String url;

    @Range(min = 301, max = 302, message = "redirectType should be 301 or 302")
    private int redirectType;

    public UrlRegisterRequest() {
        this(null, DEFAULT_REDIRECT_TYPE); // default values
    }

    public UrlRegisterRequest(String url, int redirectType) {
        this.url = url;
        this.redirectType = redirectType;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public int getRedirectType() {
        return redirectType;
    }

    public void setRedirectType(int redirectType) {
        this.redirectType = redirectType;
    }
}
