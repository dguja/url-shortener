package urlshortener.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.util.HashSet;
import java.util.Set;

/**
 * Entity class for the account.
 */
@Entity
public class Account {

    @Id
    private String id;

    @JsonIgnore
    private String password;

    @OneToMany(mappedBy = "account")
    private Set<LongToShortUrlMapping> urlMappings = new HashSet<>();

    Account() {
    } // default constructor, JPA only

    public Account(String id, String password) {
        this.id = id;
        this.password = password;
    }

    public String getId() {
        return id;
    }

    public String getPassword() {
        return password;
    }

    public Set<LongToShortUrlMapping> getUrlMappings() {
        return urlMappings;
    }

    public void addUrlMapping(LongToShortUrlMapping urlMapping) {
        urlMappings.add(urlMapping);
    }
}
