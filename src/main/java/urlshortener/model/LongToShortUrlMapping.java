package urlshortener.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

/**
 * Class holds a mapping between short and long url.
 */
@Entity
public class LongToShortUrlMapping {

    @Id
    @GeneratedValue
    @JsonIgnore
    private Long id;

    @JsonIgnore
    @ManyToOne
    private Account account;

    private String longUrl;
    private String shortUrl; // full short url (contains a host name, port...)
    private int hitsCounter;
    private int redirectType;

    @JsonIgnore
    private String shortUrlSuffix; // generated suffix of a short url

    LongToShortUrlMapping() {
    } // default constructor, JPA only

    public LongToShortUrlMapping(Account account, String longUrl, String shortUrl, int redirectType) {
        this.account = account;
        this.longUrl = longUrl;
        this.shortUrl = shortUrl;
        this.redirectType = redirectType;
    }

    public Long getId() {
        return id;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public String getLongUrl() {
        return longUrl;
    }

    public void setLongUrl(String longUrl) {
        this.longUrl = longUrl;
    }

    public String getShortUrl() {
        return shortUrl;
    }

    public void setShortUrl(String shortUrl) {
        this.shortUrl = shortUrl;
    }

    public int getHitsCounter() {
        return hitsCounter;
    }

    public void setHitsCounter(int hitsCounter) {
        this.hitsCounter = hitsCounter;
    }

    public int getRedirectType() {
        return redirectType;
    }

    public void setRedirectType(int redirectType) {
        this.redirectType = redirectType;
    }

    public String getShortUrlSuffix() {
        return shortUrlSuffix;
    }

    public void setShortUrlSuffix(String shortUrlSuffix) {
        this.shortUrlSuffix = shortUrlSuffix;
    }

    public void increaseHitsCounter() {
        hitsCounter++;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        LongToShortUrlMapping that = (LongToShortUrlMapping) o;

        if (hitsCounter != that.hitsCounter) return false;
        if (redirectType != that.redirectType) return false;
        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (account != null ? !account.equals(that.account) : that.account != null) return false;
        if (longUrl != null ? !longUrl.equals(that.longUrl) : that.longUrl != null) return false;
        return shortUrl != null ? shortUrl.equals(that.shortUrl) : that.shortUrl == null;

    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (account != null ? account.hashCode() : 0);
        result = 31 * result + (longUrl != null ? longUrl.hashCode() : 0);
        result = 31 * result + (shortUrl != null ? shortUrl.hashCode() : 0);
        result = 31 * result + hitsCounter;
        result = 31 * result + redirectType;
        return result;
    }
}
