package urlshortener.repository;

import org.springframework.data.repository.CrudRepository;
import urlshortener.model.Account;

public interface AccountRepository extends CrudRepository<Account, String> {
}
