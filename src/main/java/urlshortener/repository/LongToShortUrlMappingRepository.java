package urlshortener.repository;

import org.springframework.data.repository.CrudRepository;
import urlshortener.model.LongToShortUrlMapping;

import java.util.Collection;

public interface LongToShortUrlMappingRepository extends CrudRepository<LongToShortUrlMapping, Long> {

    Collection<LongToShortUrlMapping> findByAccountId(String id);

    LongToShortUrlMapping findByShortUrlSuffix(String shortUrl);

    LongToShortUrlMapping findByAccountIdAndLongUrl(String id, String longUrl);
}
