package urlshortener.util;

import java.util.Random;

/**
 * String utils. Contains methods for processing strings.
 */
public class StringUtils {

    private static final String ALPHA_LOWER = "abcdefghijklmnopqrstuvwxyz";
    private static final String ALPHA_UPPER = ALPHA_LOWER.toUpperCase();
    private static final String ALPHANUMERIC = ALPHA_LOWER + ALPHA_UPPER + "0123456789";

    private static final int ALPHABET_LENGTH = ALPHANUMERIC.length();

    private static final Random random = new Random();

    /**
     * Generates random alphanumeric string.
     *
     * @param stringLength length of generated string. If smaller than 0, returns empty string
     * @return generated string
     */
    public static String generateAlphanumericString(int stringLength) {
        final StringBuilder stringBuilder = new StringBuilder();

        for (int i = 0; i < stringLength; i++) {
            stringBuilder.append(ALPHANUMERIC.charAt(random.nextInt(ALPHABET_LENGTH)));
        }

        return stringBuilder.toString();
    }

    /**
     * Encode value to base 62
     *
     * @param value to encode
     * @return encoded value
     */
    public static String encode(long value) {
        final StringBuilder sb = new StringBuilder();

        while (value > 0) {
            sb.append(ALPHANUMERIC.charAt((int) value % ALPHABET_LENGTH));
            value /= ALPHABET_LENGTH;
        }

        return sb.toString();
    }

}
